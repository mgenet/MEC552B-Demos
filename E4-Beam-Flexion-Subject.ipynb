{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "# MEC552B — Numerical methods in (solid) mechanics — Martin Genet\n",
    "\n",
    "# L4 — Partial differential equations (Elastostatics boundary value problem) — The Ritz-Galerkin method\n",
    "\n",
    "# E4 — Euler beam in flexion under gravity"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "As usual, we start with some imports…"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# For better printing within jupyter cells\n",
    "import IPython\n",
    "IPython.core.interactiveshell.InteractiveShell.ast_node_interactivity = \"all\"\n",
    "\n",
    "# Standard python libraries\n",
    "import time\n",
    "import timeit\n",
    "\n",
    "# Computing libraries\n",
    "import numpy # doc: https://numpy.org/doc/stable\n",
    "import scipy # doc: https://docs.scipy.org/doc/scipy\n",
    "import scipy.integrate # MG20221010: Needed with scipy 1.7.3…why?!\n",
    "import sympy # doc: https://docs.sympy.org\n",
    "\n",
    "# Plotting libraries\n",
    "import matplotlib.pyplot as plt # doc: https://matplotlib.org/stable/contents.html"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Problem"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "nteract": {
     "transient": {
      "deleting": false
     }
    },
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We consider a simple beam (length $L$, section area $S$, section second area moment $I$, density $\\rho$, Young modulus $E$) simply supported on both sides and subjected to gravity ($-g~\\underline{e_y}$), as represented in the following figure:\n",
    "<div style=\"text-align:center\">\n",
    "    <img src=\"FIGURES/E4-beam-supported.svg\" width=500/> <!-- MG20200603: only svg or png, no pdf.-->\n",
    "</div>\n",
    "<!-- ![](FIGURES/PC1-beam-trim.png) -->\n",
    "<!-- \\begin{figure}\n",
    "    \\centering\n",
    "    \\includegraphics[width=1cm]{FIGURES/PC1-beam-trim.png} % MG20200603: width has no effect. Only png works, no svg or pdf.\n",
    "    \\caption{\\label{fig:example} This is an example of figure included using LaTeX commands.}\n",
    "\\end{figure} -->\n",
    "\n",
    "We recall that in the framework of the [Euler-Bernoulli beam theory](https://en.wikipedia.org/wiki/Euler%E2%80%93Bernoulli_beam_theory), the two main unknowns are the deflection $w$ and the bending moment $M$. Here the kinematic conditions on the deflection $w$ are:\n",
    "$$\n",
    "    w\\left(x = 0\\right) = w\\left(x = L\\right) = 0.\n",
    "$$\n",
    "The equilibrium equations that the bending moment $M$ must satisfy are:\n",
    "$$\n",
    "    \\begin{cases}\n",
    "        M_{,xx} + \\rho S g = 0 \\\\\n",
    "        M\\left(x = 0\\right) = M\\left(x = L\\right) = 0\n",
    "    \\end{cases}.\n",
    "$$\n",
    "Finally, the constitutive relation linking the deflection and the bending moment is:\n",
    "$$\n",
    "    \\begin{cases}\n",
    "        M = E I \\kappa \\\\\n",
    "        \\kappa = \\omega_{,x} \\\\\n",
    "        \\omega = w_{,x}\n",
    "    \\end{cases},\n",
    "$$\n",
    "where $\\kappa$ is the bending deformation and $\\omega$ the rotation of the beam, i.e.,\n",
    "$$\n",
    "    M = E I w_{,xx} .\n",
    "$$\n",
    "\n",
    "(Note that in the previous equations, $._{,x}$ & $._{,xx}$ denote the first and second spatial derivatives.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Exact solution"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "As usual, since it is tractable we start by calculating the exact solution of the problem, which we will later use to validate our numerical approximation method and its implementation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q1.\n",
    "What is the exact solution (in terms of both $w$ & $M$) of the problem?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now compute this exact solution symbolically using `sympy`.\n",
    "We first define symbolic variables, as well as their numerical values that we store into a python dictionary for future numerical applications."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:22.298Z",
     "iopub.status.busy": "2020-04-05T15:40:22.295Z",
     "iopub.status.idle": "2020-04-05T15:40:22.303Z",
     "shell.execute_reply": "2020-04-05T15:40:22.306Z"
    }
   },
   "outputs": [],
   "source": [
    "x   = sympy.Symbol('x')\n",
    "L   = sympy.Symbol('L')\n",
    "S   = sympy.Symbol('S')\n",
    "I   = sympy.Symbol('I')\n",
    "rho = sympy.Symbol('rho')\n",
    "E   = sympy.Symbol('E')\n",
    "g   = sympy.Symbol('g')\n",
    "\n",
    "w = sympy.Function('w')(x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "L_   = 1.\n",
    "S_   = 1.\n",
    "I_   = 1.\n",
    "rho_ = 1.\n",
    "E_   = 1.\n",
    "g_   = 1.\n",
    "\n",
    "subs_dict      = {}\n",
    "subs_dict[L]   = L_\n",
    "subs_dict[S]   = S_\n",
    "subs_dict[I]   = I_\n",
    "subs_dict[rho] = rho_\n",
    "subs_dict[E]   = E_\n",
    "subs_dict[g]   = g_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now integrate the differential equation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q2.\n",
    "Complete and execute the following code.**\n",
    "\n",
    "Hints:\n",
    "* In `sympy` the derivative of an expression `f` with respect to a variable `x` is `f.diff(x)` or `sympy.Derivative(f,x)`.\n",
    "The second derivative is `f.diff(x).diff(x)`, `f.diff(x, x)` or `sympy.Derivative(f, x, x)`.\n",
    "* `sympy.dsolve(eq, func, ics)` solves the differential equation `eq = 0` (no need to specify `= 0` in `sympy.dsolve`) for the function `func` with initial conditions `ics` (initial conditions such as $f(x=0) = f'(x=0) = 0$ would be expressed as `{f.subs(x,0):0, f.diff(x).subs(x,0):0}`).\n",
    "(Note that it returns an equation `func = …`, hence the need for `rhs` —right hand side— to extract the solution itself.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:25.733Z",
     "iopub.status.busy": "2020-04-05T15:40:25.693Z",
     "iopub.status.idle": "2020-04-05T15:40:28.211Z",
     "shell.execute_reply": "2020-04-05T15:40:28.233Z"
    }
   },
   "outputs": [],
   "source": [
    "omega = ### YOUR CODE HERE ###\n",
    "print(\"omega:\"); omega\n",
    "kappa = ### YOUR CODE HERE ###\n",
    "print(\"kappa:\"); kappa\n",
    "M = ### YOUR CODE HERE ###\n",
    "print(\"M:\"); M\n",
    "w_sol = sympy.dsolve(\n",
    "    eq=### YOUR CODE HERE ###,\n",
    "    func=### YOUR CODE HERE ###,\n",
    "    ics={### YOUR CODE HERE ###}).rhs\n",
    "print(\"w_sol:\"); w_sol\n",
    "M_sol = M.subs(w, w_sol).doit()\n",
    "print(\"M_sol:\"); M_sol"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "And plot the solution, in terms of deflection $w$ and bending moment $M$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:28.229Z",
     "iopub.status.busy": "2020-04-05T15:40:28.216Z",
     "iopub.status.idle": "2020-04-05T15:40:28.539Z",
     "shell.execute_reply": "2020-04-05T15:40:28.562Z"
    }
   },
   "outputs": [],
   "source": [
    "w_sol_ = w_sol.subs(subs_dict)\n",
    "w_sol_plot = sympy.plot(\n",
    "    w_sol_,\n",
    "    (x,0,L_),\n",
    "    xlabel=\"x\",\n",
    "    ylabel=\"w(x)\")\n",
    "M_sol_ = M_sol.subs(subs_dict)\n",
    "M_sol_plot = sympy.plot(\n",
    "    M_sol_,\n",
    "    (x,0,L_),\n",
    "    xlabel=\"x\",\n",
    "    ylabel=\"M(x)\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Approximated solution using the Ritz-Galerkin method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now compute an approximated solution of the problem using the Ritz-Galerkin method, which we will compare to the exact solution to verify our implementation and to investigate the impact of the various parameters of the method."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q3.\n",
    "Derive the weak formulation of the problem from the strong formulation given above, following the Galerkin procedure.**\n",
    "\n",
    "Hints:\n",
    " * Follow the Galerkin procedure to turn the local equations (equilibrium, behavior, boundary conditions) into a single global variational equation of the form $w ~/~ W_\\text{int}\\left(w, w^*\\right) = W_\\text{ext}\\left(w^*\\right) ~ \\forall w^*$, where $W_\\text{int}$ is a bilinear symmetric coercive form (which, in mechanics, corresponds to the virtual work of internal forces) and $W_\\text{ext}$ is a linear form (which, in mechanics, corresponds to the virtual work of external forces), both to be expressed in terms of the problem parameters.\n",
    " * Here the equilibrium equation is second order, so two integrations by parts are needed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "Following the Ritz procedure, we will now inject in this weak formulation a linear combination of shape functions, and turn the continuous problem into a discrete one.\n",
    "In a first time, we consider shape functions that already verify the boundary conditions, sine functions.\n",
    "We define them symbolically using `sympy`, so we can integrate them analytically (which makes things easy, but potentially slow, as we will see later on)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q4.\n",
    "Define the list of $N$ (we will start with $N=1$ but the code needs to be generic) first sine functions (as `sympy.Function` objects) verifying the boundary conditions.**\n",
    "\n",
    "Hints:\n",
    "* `sympy.sin(sympy.pi*x/L)` corresponds to $\\sin\\left(\\pi \\frac{x}{L}\\right)$.\n",
    "* `[i for i in range(n)]` returns the list `[0, 1, …, n-1]`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:28.557Z",
     "iopub.status.busy": "2020-04-05T15:40:28.554Z",
     "iopub.status.idle": "2020-04-05T15:40:40.192Z",
     "shell.execute_reply": "2020-04-05T15:40:40.209Z"
    }
   },
   "outputs": [],
   "source": [
    "def build_shape_functions(n_phi):\n",
    "    return [### YOUR CODE HERE ###]\n",
    "\n",
    "phi_list = build_shape_functions(n_phi=1)\n",
    "print(\"phi_list:\"); phi_list"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now build the stiffness matrix (as a 2D `numpy.ndarray`) of the problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q5.\n",
    "What is the expression of the term $K_{ij}$ of the stiffness matrix?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q6.\n",
    "Is $\\underline{\\underline{\\mathbb{K}}}$ symmetric?\n",
    "Why?\n",
    "Is that always the case in the Ritz-Galerkin method?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q7.\n",
    "Is $\\underline{\\underline{\\mathbb{K}}}$ diagonal?\n",
    "Why?\n",
    "Is that always the case in the Ritz-Galerkin method?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We propose two different functions for building the problem stiffness matrix, to compare symbolic & numerical (i.e., quadrature) integration.\n",
    "\n",
    "In the quadrature function, we also compare the case where we \"compile\" (here using the [`sympy.lambdify`](https://docs.sympy.org/latest/modules/utilities/lambdify.html) framework) the integrand before providing it to the quadrature function (here the [`scipy.integrate.fixed_quad`](https://docs.scipy.org/doc/scipy/reference/generated/scipy.integrate.fixed_quad.html) function) to the case where we simply perform symbolic substitution (here using the [`subs`](https://docs.sympy.org/latest/tutorials/intro-tutorial/basic_operations.html) function).\n",
    "Indeed, quadrature requires many function evaluations, so that it is critical to perform such evaluations as efficiently as possible.\n",
    "\n",
    "Nevertheless, notice how the use of symbolic variables makes it simple to express the integrand, independently from the integration method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:28.557Z",
     "iopub.status.busy": "2020-04-05T15:40:28.554Z",
     "iopub.status.idle": "2020-04-05T15:40:40.192Z",
     "shell.execute_reply": "2020-04-05T15:40:40.209Z"
    }
   },
   "outputs": [],
   "source": [
    "def build_stiffness_matrix_with_integration(phi_list):\n",
    "    n_phi = len(phi_list)\n",
    "    K = numpy.zeros((n_phi,n_phi))\n",
    "    for i_phi in range(n_phi):\n",
    "        # Symbolic expression of the integrand, using our symbolic framework\n",
    "        #  (Remember that M & kappa involve derivatives of w.)\n",
    "        Kii = M.subs(w, phi_list[i_phi]).doit() * kappa.subs(w, phi_list[i_phi]).doit()\n",
    "        # Symbolic integration, followed by substitution (of our symbolic parameters)\n",
    "        #  and evaluation (to also convert symbolic expressions such as pi, sin, etc.)\n",
    "        #  to obtain a floating point number\n",
    "        K[i_phi,i_phi] = sympy.integrate(\n",
    "            Kii,\n",
    "            (x, 0, L)).subs(subs_dict).evalf()\n",
    "    return K\n",
    "\n",
    "def build_stiffness_matrix_with_quadrature(phi_list, quad_order=1, use_lambdify=True):\n",
    "    n_phi = len(phi_list)\n",
    "    K = numpy.zeros((n_phi,n_phi))\n",
    "    for i_phi in range(n_phi):\n",
    "        # Symbolic expression of the integrand, using our symbolic framework\n",
    "        #  (Remember that M & kappa involve derivatives of w.)\n",
    "        #  (Here we directly substitute the symbolic parameters,\n",
    "        #   such that the expression is only of function of x)\n",
    "        Kii = (M.subs(w, phi_list[i_phi]).doit() * kappa.subs(w, phi_list[i_phi]).doit()).subs(subs_dict)\n",
    "        if (use_lambdify):\n",
    "            # Compilation of the expression into a function that is fast to evaluate\n",
    "            #  (Note that lambdify functions can take single values or list of values;\n",
    "            #   i.e., it can evaluate the function at one point or many points at once.)\n",
    "            Kii_ = sympy.lambdify(\n",
    "                args=x,\n",
    "                expr=Kii)\n",
    "        else:\n",
    "            # Simple function that evaluates the expression using subs followed by evalf\n",
    "            #  (Note that scipy.integrate.fixed_quad evaluates all quadrature points at once,\n",
    "            #   so the function must be able to handle a list of values as input\n",
    "            #   and return a list of values as output.)\n",
    "            Kii_ = lambda x_: [Kii.subs(x,x__).evalf() for x__ in x_]\n",
    "        # Numerical integration, i.e., quadrature\n",
    "        #  (Here we use Gauss quadrature of a given order.)\n",
    "        K[i_phi,i_phi] = scipy.integrate.fixed_quad(\n",
    "            func=Kii_,\n",
    "            a=0.,\n",
    "            b=L_,\n",
    "            n=quad_order)[0]\n",
    "    return K\n",
    "\n",
    "phi_list = build_shape_functions(n_phi=1)\n",
    "K = build_stiffness_matrix_with_integration(phi_list)\n",
    "print(\"K:\"); K\n",
    "K = build_stiffness_matrix_with_quadrature(phi_list, quad_order=6, use_lambdify=0)\n",
    "print(\"K:\"); K\n",
    "K = build_stiffness_matrix_with_quadrature(phi_list, quad_order=6, use_lambdify=1)\n",
    "print(\"K:\"); K"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "Let us study the quadrature error as a function of the quadrature order.\n",
    "(Do not hesitate to modify the error metric and plot representation.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "phi_list = build_shape_functions(n_phi=1)\n",
    "K_int = build_stiffness_matrix_with_integration(phi_list)\n",
    "\n",
    "order_list = list(range(1,10))\n",
    "error_without_lambdify_list = []\n",
    "error_with_lambdify_list = []\n",
    "\n",
    "for order in order_list:\n",
    "    K_qua = build_stiffness_matrix_with_quadrature(phi_list, quad_order=order, use_lambdify=0)\n",
    "    error_without_lambdify_list.append((K_qua[0,0] - K_int[0,0])/K_int[0,0])\n",
    "    K_qua = build_stiffness_matrix_with_quadrature(phi_list, quad_order=order, use_lambdify=1)\n",
    "    error_with_lambdify_list.append((K_qua[0,0] - K_int[0,0])/K_int[0,0])\n",
    "\n",
    "# We plot the convergence\n",
    "plt.plot(order_list, error_without_lambdify_list, label=\"without lambdify\")\n",
    "plt.plot(order_list, error_with_lambdify_list, label=\"with lambdify\")\n",
    "plt.xlabel(\"quadrature order\");\n",
    "plt.ylabel(\"integration relative error\");\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q8. What do you notice? I.e., from what order is the quadrature satisfying? Was that expected? Is there a difference between using or not lambdify? Any other remark?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "Let us now study the integration time.\n",
    "Since execution time can depend on many things such as current processor and memory load, here we use the [`timeit`](https://docs.python.org/3/library/timeit.html) module, which runs the function multiple times and return the average run time.\n",
    "(Again, do not hesitate to modify the plot.)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "phi_list = build_shape_functions(n_phi=1)\n",
    "\n",
    "timeit_n = 10\n",
    "int_time = timeit.timeit(\"build_stiffness_matrix_with_integration(phi_list)\", number=timeit_n, globals=globals())\n",
    "\n",
    "order_list = list(range(1,50))\n",
    "quad_time_without_lamdify_list = []\n",
    "quad_time_with_lamdify_list = []\n",
    "\n",
    "for order in order_list:\n",
    "    qua_time = timeit.timeit(\"build_stiffness_matrix_with_quadrature(phi_list, quad_order=order, use_lambdify=0)\", number=timeit_n, globals=globals())\n",
    "    quad_time_without_lamdify_list.append(qua_time)\n",
    "    qua_time = timeit.timeit(\"build_stiffness_matrix_with_quadrature(phi_list, quad_order=order, use_lambdify=1)\", number=timeit_n, globals=globals())\n",
    "    quad_time_with_lamdify_list.append(qua_time)\n",
    "\n",
    "# We plot the convergence\n",
    "plt.axhline(int_time, color=\"black\", label=\"integration time\")\n",
    "plt.plot(order_list, quad_time_without_lamdify_list, label=\"quadrature time (without lambdify)\")\n",
    "plt.plot(order_list, quad_time_with_lamdify_list, label=\"quadrature time (with lambdify)\")\n",
    "plt.xlabel(\"quadrature order\");\n",
    "plt.ylabel(\"time (s)\");\n",
    "plt.yscale(\"log\"); # This might be useful…or not!\n",
    "plt.legend();"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q9. What do you notice? Was that expected?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now build the force vector (as a 1D `numpy.ndarray`) of the problem."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q10.\n",
    "What is the expression of the term $F_{i}$ of the force vector?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q11.\n",
    "Complete and execute the following code.**\n",
    "\n",
    "Hints:\n",
    "* `numpy.zeros(n)` returns a 1D array of size n, full of zeros.\n",
    "* `scipy.integrate.fixed_quad(f, a, b, n)` approximates $\\int_a^b f$ through Gauss quadrature of order n. `f` must be a python function taking a point/list of points as input(s), and returning the value/list of values of the function at these points."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:28.557Z",
     "iopub.status.busy": "2020-04-05T15:40:28.554Z",
     "iopub.status.idle": "2020-04-05T15:40:40.192Z",
     "shell.execute_reply": "2020-04-05T15:40:40.209Z"
    }
   },
   "outputs": [],
   "source": [
    "def build_force_vector(phi_list, quad_order=1):\n",
    "    n_phi = len(phi_list)\n",
    "    F = ### YOUR CODE HERE ###\n",
    "    for i_phi in range(n_phi):\n",
    "        Fi  = ### YOUR CODE HERE ###\n",
    "        Fi_ = sympy.lambdify(\n",
    "            args=x,\n",
    "            expr=Fi)\n",
    "        F[i_phi] = scipy.integrate.fixed_quad(\n",
    "            func=### YOUR CODE HERE ###,\n",
    "            a=### YOUR CODE HERE ###,\n",
    "            b=### YOUR CODE HERE ###,\n",
    "            n=### YOUR CODE HERE ###)[0]\n",
    "    return F\n",
    "\n",
    "phi_list = build_shape_functions(n_phi=1)\n",
    "F = build_force_vector(phi_list, quad_order=6)\n",
    "print(\"F:\"); F"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now solve the linear system."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q12.\n",
    "Complete and execute the following code**\n",
    "\n",
    "Hint:\n",
    "* `numpy.linalg.solve(A, B)` will return the solution of $\\underline{\\underline{\\mathbb{A}}} \\cdot \\underline{\\mathbb{X}} = \\underline{\\mathbb{B}}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:28.557Z",
     "iopub.status.busy": "2020-04-05T15:40:28.554Z",
     "iopub.status.idle": "2020-04-05T15:40:40.192Z",
     "shell.execute_reply": "2020-04-05T15:40:40.209Z"
    }
   },
   "outputs": [],
   "source": [
    "phi_list = build_shape_functions(n_phi=1)\n",
    "K = ### YOUR CODE HERE ###\n",
    "F = ### YOUR CODE HERE ###\n",
    "U = ### YOUR CODE HERE ###\n",
    "print(\"U:\"); U"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now build the approximated solution as a linear combination of the shape functions, the weights having just been calculated."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q13.\n",
    "Complete and execute the following code.\n",
    "`w_app_` and `M_app_` must be functions of `x` only.**\n",
    "\n",
    "Hint:\n",
    "* `numpy.dot(a,b)` return the dot product of the two 1D arrays `a` & `b`, i.e., $\\sum_i a_i b_i$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:28.557Z",
     "iopub.status.busy": "2020-04-05T15:40:28.554Z",
     "iopub.status.idle": "2020-04-05T15:40:40.192Z",
     "shell.execute_reply": "2020-04-05T15:40:40.209Z"
    }
   },
   "outputs": [],
   "source": [
    "w_app_ = ### YOUR CODE HERE ###\n",
    "print(\"w_app_:\"); w_app_\n",
    "M_app_ = M.subs(w, w_app_).doit().subs(subs_dict)\n",
    "print(\"M_app_:\"); M_app_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We now plot the approximated solution (in red) and compare it to the exact solution (in blue), in terms of deflection $w$ and bending moment $M$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "execution": {
     "iopub.execute_input": "2020-04-05T15:40:40.200Z",
     "iopub.status.busy": "2020-04-05T15:40:40.197Z",
     "iopub.status.idle": "2020-04-05T15:40:40.528Z",
     "shell.execute_reply": "2020-04-05T15:40:40.537Z"
    }
   },
   "outputs": [],
   "source": [
    "w_app_plot = sympy.plot(\n",
    "    w_app_,\n",
    "    (x,0,L_),\n",
    "    line_color=\"red\",\n",
    "    xlabel=\"x\",\n",
    "    ylabel=\"w(x)\",\n",
    "    show=False)\n",
    "w_app_plot.extend(w_sol_plot)\n",
    "w_app_plot.show()\n",
    "M_app_plot = sympy.plot(\n",
    "    M_app_,\n",
    "    (x,0,L_),\n",
    "    line_color=\"red\",\n",
    "    xlabel=\"x\",\n",
    "    ylabel=\"M(x)\",\n",
    "    show=False)\n",
    "M_app_plot.extend(M_sol_plot)\n",
    "M_app_plot.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We can also compute the normalized error, i.e.,\n",
    "$$\n",
    "err\n",
    "= \\frac{\\sqrt{\\frac{1}{L}\\int_0^L \\left( w_\\text{app} - w_\\text{sol}\\right)^2}}{\\sqrt{\\frac{1}{L}\\int_0^L w_\\text{sol}^2}},\n",
    "$$\n",
    "either symbolically or numerically."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def compute_error_using_integration():\n",
    "    w_app_err_  = ((1/L_) * sympy.integrate(\n",
    "        (w_app_ - w_sol_)**2,\n",
    "        (x, 0, L_)).evalf())**(0.5)\n",
    "    w_app_err_ /= ((1/L_) * sympy.integrate(\n",
    "        w_sol_**2,\n",
    "        (x, 0, L_)).evalf())**(0.5)\n",
    "    return w_app_err_\n",
    "\n",
    "def compute_error_using_quadrature(quad_order):\n",
    "    f_ = sympy.lambdify(\n",
    "        args=x,\n",
    "        expr=(w_app_ - w_sol_)**2)\n",
    "    w_app_err_ = ((1/L_) * scipy.integrate.fixed_quad(\n",
    "        func=f_, a=0., b=L_,\n",
    "        n=quad_order)[0])**(0.5)\n",
    "    g_ = sympy.lambdify(\n",
    "        args=x,\n",
    "        expr=w_sol_**2)\n",
    "    w_app_err_ /= ((1/L_) * scipy.integrate.fixed_quad(\n",
    "        func=g_, a=0., b=L_,\n",
    "        n=quad_order)[0])**(0.5)\n",
    "    return w_app_err_\n",
    "\n",
    "w_app_err_ = compute_error_using_integration() # This takes forever for N > 1, just comment it out!\n",
    "print(\"w_app_err_:\"); w_app_err_\n",
    "w_app_err_ = compute_error_using_quadrature(quad_order=8) # Whereas this is always quite fast.\n",
    "print(\"w_app_err_:\"); w_app_err_\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q14.\n",
    "What do you think about the approximation?\n",
    "Is it reasonable?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q15.\n",
    "Increase the number of shape functions to 2.\n",
    "Does that improve the solution?\n",
    "Why?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q16.\n",
    "Increase the number of shape functions, to 3, 4, etc.\n",
    "What do you notice?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Case of clamped boundary conditions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We will now consider clamped instead of simply supported boundary conditions."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q17.\n",
    "How is the problem changed?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We start by computing the exact solution using `sympy`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q18.\n",
    "Duplicate and modify the code of the Section 2 to compute and plot the exact solution.**"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "### YOUR CODE HERE ###\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "We then compute an approximated solution with the Ritz-Galerkin method.\n",
    "\n",
    "The main difference with Section 3 is that our shape functions do not verify (all) the boundary conditions, so that (some) boundary conditions need to be enforced on the approximated solution, which is done at the linear system level as seen in L1 & E1.\n",
    "(Of course we could use new shape functions that verify the new boundary conditions, but let us keep the sine functions to emphasize the general handling of boundary conditions.)\n",
    "We will use the penalization method.\n",
    "Let us express the (linear) boundary conditions to enforce in a general way:\n",
    "$$\n",
    "    \\mathbb{\\bar C} \\cdot \\mathbb{U} = \\mathbb{\\bar U}\n",
    "$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q19. What is the size of arrays $\\mathbb{\\bar C}$ & $\\mathbb{\\bar U}$?\n",
    "What is their expression?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q20.\n",
    "How is the linear system modified?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q21.\n",
    "Duplicate and modify the code of the Section 3 to compute and plot the approximated solution.**\n",
    "\n",
    "Hint:\n",
    "* When filling the $\\mathbb{\\bar C}$ array, it is convenient to use the symbolic shape functions stored in `phi_list`, as we do for the $\\mathbb{K}$ & $\\mathbb{F}$ arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "### YOUR CODE HERE ###\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q22.\n",
    "What do you notice?**"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "## Bonus"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "deletable": false,
    "editable": false,
    "run_control": {
     "frozen": true
    }
   },
   "source": [
    "**Q23.\n",
    "Propose and implement another family of shape functions.**"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.8.13 ('MEC552')",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  },
  "latex_envs": {
   "LaTeX_envs_menu_present": false,
   "autoclose": false,
   "autocomplete": false,
   "bibliofile": "biblio.bib",
   "cite_by": "apalike",
   "current_citInitial": 1,
   "eqLabelWithNumbers": true,
   "eqNumInitial": 1,
   "hotkeys": {
    "equation": "Ctrl-E",
    "itemize": "Ctrl-I"
   },
   "labels_anchors": false,
   "latex_user_defs": true,
   "report_style_numbering": false,
   "user_envs_cfg": false
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": true,
   "sideBar": true,
   "skip_h1_title": true,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {
    "height": "calc(100% - 180px)",
    "left": "10px",
    "top": "150px",
    "width": "165px"
   },
   "toc_section_display": true,
   "toc_window_display": true
  },
  "varInspector": {
   "cols": {
    "lenName": 16,
    "lenType": 16,
    "lenVar": 40
   },
   "kernels_config": {
    "python": {
     "delete_cmd_postfix": "",
     "delete_cmd_prefix": "del ",
     "library": "var_list.py",
     "varRefreshCmd": "print(var_dic_list())"
    },
    "r": {
     "delete_cmd_postfix": ") ",
     "delete_cmd_prefix": "rm(",
     "library": "var_list.r",
     "varRefreshCmd": "cat(var_dic_list()) "
    }
   },
   "oldHeight": 482.48366599999997,
   "position": {
    "height": "40px",
    "left": "1590.83px",
    "right": "20px",
    "top": "121px",
    "width": "311.1px"
   },
   "types_to_exclude": [
    "module",
    "function",
    "builtin_function_or_method",
    "instance",
    "_Feature"
   ],
   "varInspector_section_display": "none",
   "window_display": false
  },
  "vscode": {
   "interpreter": {
    "hash": "96aa456a741804e957593b3d1f005f59862697d834b323ad509ac5340fbdf582"
   }
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
